using UniversityMvcApp.Models;

namespace UniversityMvcApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<UniversityMvcApp.Models.UniversityDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(UniversityMvcApp.Models.UniversityDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            //context.Semesters.AddOrUpdate(
            //    new Semester { SemesterNumber = 1 },
            //    new Semester { SemesterNumber = 2 },
            //    new Semester { SemesterNumber = 3 },
            //    new Semester { SemesterNumber = 4 },
            //    new Semester { SemesterNumber = 5 },
            //    new Semester { SemesterNumber = 6 },
            //    new Semester { SemesterNumber = 7 },
            //    new Semester { SemesterNumber = 8 }

            //    );

            //context.Designations.AddOrUpdate(
            //    new Designation { DesignationName = "Professor" },
            //    new Designation { DesignationName = "Assistant Professor" },
            //    new Designation { DesignationName = "Associate Professor" },
            //    new Designation { DesignationName = "Lecturer" },
            //    new Designation { DesignationName = "Assistant Lecturer" }
            //    );


            //context.Rooms.AddOrUpdate(
            //               new Room { RoomNo = "101" },
            //               new Room { RoomNo = "102" },
            //               new Room { RoomNo = "103" },
            //               new Room { RoomNo = "104" },
            //               new Room { RoomNo = "105" },
            //               new Room { RoomNo = "201" },
            //               new Room { RoomNo = "202" },
            //               new Room { RoomNo = "203" },
            //               new Room { RoomNo = "204" },
            //               new Room { RoomNo = "205" },
            //               new Room { RoomNo = "301" },
            //               new Room { RoomNo = "302" },
            //               new Room { RoomNo = "303" },
            //               new Room { RoomNo = "304" },
            //               new Room { RoomNo = "305" },
            //               new Room { RoomNo = "401" },
            //               new Room { RoomNo = "402" },
            //               new Room { RoomNo = "403" },
            //               new Room { RoomNo = "404" },
            //               new Room { RoomNo = "405" }
            //               );

        }
    }
}
