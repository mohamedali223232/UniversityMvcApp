namespace UniversityMvcApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v11 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Rooms", "DepartmentId", "dbo.Departments");
            DropIndex("dbo.Rooms", new[] { "DepartmentId" });
            AlterColumn("dbo.Rooms", "DepartmentId", c => c.Int());
            CreateIndex("dbo.Rooms", "DepartmentId");
            AddForeignKey("dbo.Rooms", "DepartmentId", "dbo.Departments", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rooms", "DepartmentId", "dbo.Departments");
            DropIndex("dbo.Rooms", new[] { "DepartmentId" });
            AlterColumn("dbo.Rooms", "DepartmentId", c => c.Int(nullable: false));
            CreateIndex("dbo.Rooms", "DepartmentId");
            AddForeignKey("dbo.Rooms", "DepartmentId", "dbo.Departments", "ID");
        }
    }
}
