﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UniversityMvcApp.BLL;
using UniversityMvcApp.Models;

namespace UniversityMvcApp.Controllers
{
    public class StudentController : Controller
    {
        StudentManager studentManager = new StudentManager();
        private UniversityDbContext db = new UniversityDbContext();

        public ActionResult Index()
        {
            return View(db.Students.ToList());
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           Student student = db.Students.Find(id);
           var departments = db.Departments.ToList();
           Department Department = departments.Find(a=>a.ID==student.DepartmentId);
           student.Department.Name = Department.Name;
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }
         public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "ID", "Code");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Student student)
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "ID", "Code");
            student.RegistrationSerial = studentManager.GetRegistration(student);
            if (ModelState.IsValid)
            {
                db.Students.Add(student);
                db.SaveChanges();
                return RedirectToAction("Details", new { id=student.Id });
            }
            ViewBag.Message = "Saved successfully";
            return View(student);
        }

      
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
